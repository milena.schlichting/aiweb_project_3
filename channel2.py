## channel.py - a simple hangman channel
##
from flask import Flask, request, render_template, jsonify
import json
import requests
import datetime
import random


# Class-based application configuration
class ConfigClass(object):
    """ Flask application config """

    # Flask settings
    SECRET_KEY = 'This is an INSECURE secret!! DO NOT use this in production!!'

# Create Flask app
app = Flask(__name__)
app.config.from_object(__name__ + '.ConfigClass')  # configuration
app.app_context().push()  # create an app context before initializing db

HUB_URL =  'https://temporary-server.de/' #'http://localhost:5555'
HUB_AUTHKEY = 'Crr-K3d-2N' #'1234567890'
CHANNEL_AUTHKEY = '22334455'
CHANNEL_NAME = "Hangman"
CHANNEL_ENDPOINT = "http://vm322.rz.uni-osnabrueck.de/user101/aiweb_project_3/channel2.wsgi"# "http://localhost:5002"
CHANNEL_FILE = 'hangman_messages.json'
GAME_FILE = 'hangman_game.json'

def read_messages():
    global CHANNEL_FILE
    try:
        f = open(CHANNEL_FILE, 'r')
    except FileNotFoundError:
        return []
    try:
        messages = json.load(f)
    except json.decoder.JSONDecodeError:
        messages = []
    f.close()
    return messages

def save_messages(messages):
    global CHANNEL_FILE
    with open(CHANNEL_FILE, 'w') as f:
        json.dump(messages, f)

def first_message():
    messages = read_messages()
    message_timestamp = datetime.datetime.now()
    formatted_timestamp = message_timestamp.strftime("%Y-%m-%d %H:%M:%S")
    message_sender = "Hangman"
    message_content = "Hehehe I bet you can't guess my word! Type 'new game' to start playing or 'stop game' if you are too scared and want to stop. Let's have some fun!"
    messages.append({'content':message_content, 'sender':message_sender, 'timestamp':formatted_timestamp})
    save_messages(messages)
    return 

#initialize game file with a "closed" game
try:
    with open('hangman_game.json', 'r') as f:
        pass      
except FileNotFoundError:
    initial_game_data = [{'word': '', 'lives':8, 'used_letters':[], 'guess_status': '', 'game_on':False}]
    with open(GAME_FILE, 'w') as f:
        json.dump(initial_game_data, f)
#initialize message file with info message
try:
    with open('hangman_messages.json', 'r') as f:
        pass      
except:
    first_message()


def hangman():
    """Commands: 'new game' starts a new game with a random word, 'stop game' stops the current game.
    Checks if guessed letters are in the word and keeps track of lives and used letters."""

    global GAME_FILE

    #get user message
    messages = read_messages()
    last_message = messages[-1]
    user_message = last_message["content"].lower()
    
    game_data = read_hangman_status()
    
    #starting new game
    if user_message == "new game":
        #stop last game
        try:
            last_game = game_data[-1]
            last_game['game_on'] = False
            save_hangman_satus(game_data)
        except:
            print("No previous game")

        #create new game
        with open('static/hangman_words.json', 'r') as file:
            word_data = json.load(file)
            word = random.choice(word_data)
        lives = 8
        used_letters = []
        guess_status = f"{'_ ' * len(word)}"

        #save the game status
        game_data.append({'word': word, 'lives':lives, 'used_letters':used_letters, 'guess_status': guess_status, 'game_on':True})
        save_hangman_satus(game_data)
        
        #define message
        message_content = f"I am thinking of a word with {len(word)} letters: {guess_status}. Take a guess! If you want to stop playing type 'stop game'."
    
    # stop game
    elif user_message == "stop game":
        try:
            last_game = game_data[-1]
            last_game['game_on'] = False
            message_content = "I stopped the game. You can start a new one with 'new game'!"
        except:
            message_content = "There is no game. But you can start one with 'new game'!"
        save_hangman_satus(game_data)

    #check if there is a current game running
    elif game_data[-1] and game_data[-1].get('game_on', None) == True:
        current_game = game_data[-1]

        #user guessed complete word
        if user_message == current_game['word']:
            message_content = f"Wow not bad! You guessed my word  correctly. You still had {current_game['lives']} lives left."
            game_data.append({'word': current_game['word'], 'lives':current_game['lives'], 'used_letters':current_game['used_letters'], 'guess_status': current_game['guess_status'], 'game_on':False})

        #user guesses a letter
        elif user_message.isalpha() and len(user_message) == 1:
            
            #repeated guess
            if user_message in current_game['used_letters']:
                message_content = f"You already tried the letter {user_message}! My word: {current_game['guess_status']}"
            
            #successful guess
            elif user_message in current_game['word']:

                used_letters = current_game.get('used_letters', [])
                used_letters.append(user_message)
                current_game['used_letters'] = used_letters

                spaced_word = ' '.join(current_game['word'])
                updated_guess_status = ''.join(char_word if char_guess == "_" and char_word == user_message else char_guess for char_word, char_guess in zip(spaced_word, current_game['guess_status']))
                
                #win game
                if updated_guess_status == spaced_word:
                    message_content = f"Hurray! You guessed my word correctly. It was {current_game['word']}. You had {current_game['lives']} lives left."
                    game_data.append({'word': current_game['word'], 'lives':current_game['lives'], 'used_letters':current_game['used_letters'], 'guess_status': updated_guess_status, 'game_on':False})
                #correct guess
                else:
                    game_data.append({'word': current_game['word'], 'lives':current_game['lives'], 'used_letters':current_game['used_letters'], 'guess_status': updated_guess_status, 'game_on':True})
                    message_content = f"Yes, {user_message} is in my word: {updated_guess_status}"
            
            # unsuccessful guess
            else:
                #track lives
                lives = current_game['lives']
                lives -= 1
                current_game['lives'] = lives

                #lost game
                if lives == 0:
                    message_content = f"You lost! My word was '{current_game['word']}'."
                    game_data.append({'word': current_game['word'], 'lives':lives, 'used_letters':current_game['used_letters'], 'guess_status': current_game['guess_status'], 'game_on':False})
                #unsuccessful guess
                else:
                    used_letters = current_game.get('used_letters', [])
                    used_letters.append(user_message)
                    current_game['used_letters'] = used_letters
                    if lives == 1:
                        message_content= f"Nope, try again. You only have 1 live left. My word: {current_game['guess_status']}"
                    else:
                        message_content= f"Nope, try again. You have {current_game['lives']} lives left. My word: {current_game['guess_status']}"
                        game_data.append({'word': current_game['word'], 'lives':lives, 'used_letters':current_game['used_letters'], 'guess_status': current_game['guess_status'], 'game_on':True})
            save_hangman_satus(game_data)
        else:
            return
    else:
        return

    #message infos ans saving
    message_timestamp = datetime.datetime.now()
    formatted_timestamp = message_timestamp.strftime("%Y-%m-%d %H:%M:%S")
    message_sender = "Hangman"
    messages.append({'content':message_content, 'sender':message_sender, 'timestamp':formatted_timestamp})
    save_messages(messages)
    return 

def save_hangman_satus(game):
    with open(GAME_FILE, 'w') as f:
        json.dump(game, f)

def read_hangman_status():
    global GAME_FILE
    try:
        f = open(GAME_FILE, 'r')
    except FileNotFoundError:
        return []
    try:
        game_data = json.load(f)
    except json.decoder.JSONDecodeError:
        game_data = []
    f.close()
    return game_data

@app.cli.command('register')
def register_command():
    global CHANNEL_AUTHKEY, CHANNEL_NAME, CHANNEL_ENDPOINT

    # send a POST request to server /channels
    response = requests.post(HUB_URL + '/channels', headers={'Authorization': 'authkey ' + HUB_AUTHKEY},
                             data=json.dumps({
            "name": CHANNEL_NAME,
            "endpoint": CHANNEL_ENDPOINT,
            "authkey": CHANNEL_AUTHKEY}))

    if response.status_code != 200:
        print("Error creating channel: "+str(response.status_code))
        return

def check_authorization(request):
    global CHANNEL_AUTHKEY
    # check if Authorization header is present
    if 'Authorization' not in request.headers:
        return False
    # check if authorization header is valid
    if request.headers['Authorization'] != 'authkey ' + CHANNEL_AUTHKEY:
        return False
    return True

@app.route('/health', methods=['GET'])
def health_check():
    global CHANNEL_NAME
    if not check_authorization(request):
        return "Invalid authorization", 400
    return jsonify({'name':CHANNEL_NAME}),  200

# GET: Return list of messages
@app.route('/', methods=['GET'])
def home_page():
    if not check_authorization(request):
        return "Invalid authorization", 400
    # fetch channels from server
    return jsonify(read_messages())

# POST: Send a message
@app.route('/', methods=['POST'])
def send_message():
    # fetch channels from server
    # check authorization header
    if not check_authorization(request):
        return "Invalid authorization", 400
    # check if message is present
    message = request.json
    if not message:
        return "No message", 400
    if not 'content' in message:
        return "No content", 400
    if not 'sender' in message:
        return "No sender", 400
    if not 'timestamp' in message:
        return "No timestamp", 400
    # add message to messages
    messages = read_messages()
    messages.append({'content':message['content'], 'sender':message['sender'], 'timestamp':message['timestamp']})
    save_messages(messages)
    if not 'sender' == "Cheesomat":
       hangman()

    return "OK", 200



# Start development web server
"""if __name__ == '__main__':
    app.run(port=5002, debug=True)"""
