Channels and client for Distributed Messanger in AI Web Course (WiSe 2024) at Uni Osnabrück

Channel: Hangman
- Write "new game" to play a game of Hangman!
- Write "stop game" to stop the game.
- source for words : https://github.com/edthrn/most-common-english-words/blob/master/scrapper.py 


Channel2: Say cheese!
 - Have a cheesy chat with the Cheesomat!
 - source for cheese facts: https://www.cheesehouse.com/uncategorized/mind-blowing-cheese-facts/ 
 - source for cheeses: https://www.cheese.com/ 

All images are taken from https://www.flaticon.com 
        bot.png by lakonicon
        cheese01.png, grim.png, send.png and info.png  by Freepik
        home.png by Dave Gandy 


Running order for local installation (don't forget to un-comment the last lines in the python files!):
1. python3 hub.py
2. python3 channel.py
3. flask --app channel.py register
4. python3 client.py