## channel.py - a simple cheese channel

from flask import Flask, request, render_template, jsonify
import json
import requests
import datetime
import random
import string
import re

# Class-based application configuration
class ConfigClass(object):
    """ Flask application config """

    # Flask settings
    SECRET_KEY = 'This is an INSECURE secret!! DO NOT use this in production!!'

# Create Flask app
app = Flask(__name__)
app.config.from_object(__name__ + '.ConfigClass')  # configuration
app.app_context().push()  # create an app context before initializing db

HUB_URL = 'https://temporary-server.de/'  #'http://localhost:5555'
HUB_AUTHKEY = 'Crr-K3d-2N' #'1234567890'
CHANNEL_AUTHKEY = '0987654321'
CHANNEL_NAME = "Say Cheese!"
CHANNEL_ENDPOINT = "http://vm322.rz.uni-osnabrueck.de/user101/aiweb_project_3/channel.wsgi" # "http://localhost:5002"
CHANNEL_FILE = 'cheese_messages.json'


def read_messages():
    global CHANNEL_FILE
    try:
        f = open(CHANNEL_FILE, 'r')
    except FileNotFoundError:
        return []
    try:
        messages = json.load(f)
    except json.decoder.JSONDecodeError:
        messages = []
    f.close()
    return messages

def save_messages(messages):
    global CHANNEL_FILE
    with open(CHANNEL_FILE, 'w') as f:
        json.dump(messages, f)

def first_message():
    messages = read_messages()
    message_timestamp = datetime.datetime.now()
    formatted_timestamp = message_timestamp.strftime("%Y-%m-%d %H:%M:%S")
    message_sender = "Cheesomat"
    message_content = "Let's talk about Cheese :)  Try typing '!suggestion'"
    messages.append({'content':message_content, 'sender':message_sender, 'timestamp':formatted_timestamp})
    save_messages(messages)
    return 

try:
    with open('cheese_messages.json', 'r') as f:
        pass      
except:
    first_message()
    print("here")

def check_for_cheese(user_message, cheese_data):
    """Checks if user input contains cheese-word."""
    for word in cheese_data:
        #filter out punctuation characters
        pattern = r'\b{}\b'.format(re.escape(word))
        if re.search(pattern, user_message, re.IGNORECASE):
            return True
    return False

def say_cheese():
    """Generates an anwer to each user message. Different answers to messages that are cheese-related, non-cheese-related or mention "Cheesomat"."""

    #list of answers for cheese-related and non-cheese-related chats
    true_answers = ["sorte","You get it!", "Ahh, I see you are a caseiculture-enthusiast as well.", "Did you know: There is no exact information regarding the origin of cheese, archaeological studies have shown the origin of cheese dates as far back as 6000 BC.", "Did you know: There are more than 2000 varieties of cheese available worldwide, mozzarella is the favorite around the globe, and the most consumed.", "Fun fact: The first cheese factory was established in Switzerland in 1815, however successful mass production began in 1851 in the United States.", "Fun fact: A whopping 20 million metric tons of cheese is produced worldwide each year and production is increasing with growing demand.", "Contrary to popular belief cheese, eaten in moderate quantities, is an excellent source of protein, calcium, and phosphorus. It’s saturated fat content is responsible for its bad reputation. So, stay healthy!", "Approximately 10 pounds of milk is required to make one pound of cheese. If it wasn’t for cheese a lot of milk would have been wasted.", "What!? You haven't heard about careale? During the Roman Empire large Roman houses had separate kitchens for manufacturing cheese only, they were called careale.", "Today, there are over over 2,000 varieties of cheeses.", "Cheese! Cheese! Cheese!", "U.S. per capita cheese consumption is about 34 pounds per person—that’s more than one full ton of cheese during the average lifetime. The French eat the most cheese, putting away an average of 57 pounds per person a year."]
    false_answers = [">:(", "Hey buddy, not around here! We want cheese!", "Talk like this doesn't brie-long here!", "I beg to disabrie!", "With talk like this you are out, so cheese wisly!", "Is all this non-cheese talk making you feel bleu? Better change it up!", "Cheddar luck next time!", "How dare you talk lactose free!", "Alarm! Alarm! We spotted a non-cheeser!", "Watch your language buddy!","Eyeyey not like this!" ]
    
    #get user messages
    messages = read_messages()
    last_message = messages[-1]
    user_message = last_message["content"].lower().split()
    
    #generate answer
    with open('static/cheeses.json', 'r') as file:
        data = json.load(file)
        if "cheesomat" in user_message:
            message_content = "That's me! :) " + random.choice(true_answers)
        elif "!suggestion" in user_message:
            message_content = "My cheesy suggestion for you is "+ string.capwords(random.choice(data)) 
        else:
            for s in user_message:
            #cheese-related 
                if check_for_cheese(s, data):
                    message_content = random.choice(true_answers)   
                    if message_content == "sorte":
                        message_content = "Have you ever tried " + string.capwords(random.choice(data)) +"?"
                    break
                #not cheese-related
                else:
                    message_content = random.choice(false_answers) + " Let's better talk about cheese! How about "+ string.capwords(random.choice(data)) + "?"
        
    # message infos and saving 
    message_timestamp = datetime.datetime.now()
    formatted_timestamp = message_timestamp.strftime("%Y-%m-%d %H:%M:%S")
    message_sender = "Cheesomat"
    messages.append({'content':message_content, 'sender':message_sender, 'timestamp':formatted_timestamp})
    save_messages(messages)
    return 


@app.cli.command('register')
def register_command():
    global CHANNEL_AUTHKEY, CHANNEL_NAME, CHANNEL_ENDPOINT

    # send a POST request to server /channels
    response = requests.post(HUB_URL + '/channels', headers={'Authorization': 'authkey ' + HUB_AUTHKEY},
                             data=json.dumps({
            "name": CHANNEL_NAME,
            "endpoint": CHANNEL_ENDPOINT,
            "authkey": CHANNEL_AUTHKEY}))

    if response.status_code != 200:
        print("Error creating channel: "+str(response.status_code))
        return

def check_authorization(request):
    global CHANNEL_AUTHKEY
    # check if Authorization header is present
    if 'Authorization' not in request.headers:
        return False
    # check if authorization header is valid
    if request.headers['Authorization'] != 'authkey ' + CHANNEL_AUTHKEY:
        return False
    return True

@app.route('/health', methods=['GET'])
def health_check():
    global CHANNEL_NAME
    if not check_authorization(request):
        return "Invalid authorization", 400
    return jsonify({'name':CHANNEL_NAME}),  200

# GET: Return list of messages
@app.route('/', methods=['GET'])
def home_page():
    if not check_authorization(request):
        return "Invalid authorization", 400
    # fetch channels from server
    return jsonify(read_messages())

# POST: Send a message
@app.route('/', methods=['POST'])
def send_message():
    # fetch channels from server
    # check authorization header
    if not check_authorization(request):
        return "Invalid authorization", 400
    # check if message is present
    message = request.json
    if not message:
        return "No message", 400
    if not 'content' in message:
        return "No content", 400
    if not 'sender' in message:
        return "No sender", 400
    if not 'timestamp' in message:
        return "No timestamp", 400
    # add message to messages
    messages = read_messages()
    messages.append({'content':message['content'], 'sender':message['sender'], 'timestamp':message['timestamp']})
    save_messages(messages)
    if not 'sender' == "Cheesomat":
       say_cheese()
       
    return "OK", 200


# Start development web server
"""if __name__ == '__main__':
    app.run(port=5001, debug=True)"""